#include <Adafruit_NeoPixel.h>
#define PIN        5
int NUMPIXELS = 74;
int half = (NUMPIXELS / 2);
//#define buttonPin   11
#define buttonPin 11        // analog input pin to use as a digital input

const int pinPote = A4;

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

const int numReadings = 15; // Higher value will result in more stability

int readings[numReadings];
int readIndex = 0;
int total = 0;
int average = 0;
int y;
int x = 35;

int estado = 0;

void setup() {
  Serial.begin(9600);
  pinMode(buttonPin, INPUT_PULLUP);
  pixels.begin();
  pixels.show();

}
void loop() {
  Serial.println(estado);
  switch (estado) {
    case 0:
      pixels.fill();
      pixels.show();
      estado = 1;
      break;

    case 1:
      leerBoton();
      break;

    case 2:

      leerBoton();
      variarColor();

      break;

    case 3:

      leerBoton();
      variarBrillo();

      break;

    case 4:

      leerBoton();
      mostrar();

      break;

    case 5:
      estado = 0;
      break;

    case 6:
      leerBoton();
      blanco();
      variarBrilloBlanco();
      break;

    case 7:
      estado = 0;
      break;

    case 8:
      leerBoton();
      primeraMitad();
      break;

    case 9:
      leerBoton();
      variarBrilloPrimeraMitad();
      break;

    case 10:
      leerBoton();
      segundaMitad();
      break;

    case 11:
      leerBoton();
      variarBrilloSegundaMitad();
      break;

    case 12:
      estado = 0;
      break;
  }

}

void leerBoton() {
  int b = checkButton();
  if (b == 1) clickEvent();
  if (b == 2) doubleClickEvent();
  if (b == 3) holdEvent();
  if (b == 4) longHoldEvent();
}

void clickEvent() {
  estado = estado + 1;
}
void doubleClickEvent() {
  estado = 6;
}
void holdEvent() {
  estado = 8;
}

void longHoldEvent() {
}



void variarColor() {

  total = total - readings[readIndex];
  readings[readIndex] = analogRead(pinPote);
  total = total + readings[readIndex];
  readIndex = readIndex + 1;

  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = total / numReadings;

  y = map(average, 0, 1024, 0, 65536);
  uint32_t rgbcolor = pixels.ColorHSV(y, 255, x);
  pixels.fill(rgbcolor);
  pixels.show();
}

void variarBrillo() {
  total = total - readings[readIndex];
  readings[readIndex] = analogRead(pinPote);
  total = total + readings[readIndex];
  readIndex = readIndex + 1;

  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = total / numReadings;

  x = map(average, 0, 1024, 0, 255);

  uint32_t rgbcolor = pixels.ColorHSV(y, 255, x);
  pixels.fill(rgbcolor);
  pixels.show();
}

void variarBrilloBlanco() {

  total = total - readings[readIndex];
  readings[readIndex] = analogRead(pinPote);
  total = total + readings[readIndex];
  readIndex = readIndex + 1;

  if (readIndex >= numReadings) {
    readIndex = 0;
  }

  average = total / numReadings;

  x = map(average, 0, 1024, 0, 255);

  uint32_t white = pixels.Color(x, x, x, 255);
  pixels.fill(white);
  pixels.show();
}

void variarBrilloPrimeraMitad() {
  total = total - readings[readIndex];
  readings[readIndex] = analogRead(pinPote);
  total = total + readings[readIndex];
  readIndex = readIndex + 1;

  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = total / numReadings;

  x = map(average, 0, 1024, 0, 255);

  uint32_t rgbcolor = pixels.ColorHSV(y, 255, x);
  half = NUMPIXELS / 2;
  pixels.fill(rgbcolor, 0, half);
  pixels.show();

}

void variarBrilloSegundaMitad() {
  total = total - readings[readIndex];
  readings[readIndex] = analogRead(pinPote);
  total = total + readings[readIndex];
  readIndex = readIndex + 1;

  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = total / numReadings;

  x = map(average, 0, 1024, 0, 255);

  uint32_t rgbcolor = pixels.ColorHSV(y, 255, x);
  half = NUMPIXELS / 2;
  pixels.fill(rgbcolor, half , half);
  pixels.show();

}
void mostrar() {
  uint32_t rgbcolor = pixels.ColorHSV(y, 255, x);
  pixels.fill(rgbcolor);
  pixels.show();
}

void blanco() {
  uint32_t white = pixels.Color(0, 0, 0, 255);
  pixels.fill(white);
  pixels.show();
}

void primeraMitad () {
  total = total - readings[readIndex];
  readings[readIndex] = analogRead(pinPote);
  total = total + readings[readIndex];
  readIndex = readIndex + 1;

  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = total / numReadings;

  y = map(average, 0, 1024, 0, 65536);
  uint32_t rgbcolor = pixels.ColorHSV(y, 255, x);

  half = NUMPIXELS / 2;
  pixels.fill(rgbcolor, 0, half);
  pixels.show();
}

void segundaMitad() {
  total = total - readings[readIndex];
  readings[readIndex] = analogRead(pinPote);
  total = total + readings[readIndex];
  readIndex = readIndex + 1;

  if (readIndex >= numReadings) {
    readIndex = 0;
  }
  average = total / numReadings;

  y = map(average, 0, 1024, 0, 65536);
  uint32_t rgbcolor = pixels.ColorHSV(y, 255, x);

  half = NUMPIXELS / 2;
  pixels.fill(rgbcolor, half , half);
  pixels.show();
}

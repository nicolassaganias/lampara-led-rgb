# lampara-led-rgb

https://www.youtube.com/watch?v=cEuYxTUpLj4

# materiales:

- tira de led ws2812b (numero de pixeles variable)
- 1 potenciometro (cualquiera va bien pero 100k ohms está bien)
- 1 boton (cualquier botón pulsador)
- 1 arduino nano
- 1 transformador 5v con + al centro y aprox 1A por metro de tira

# funcionamiento:

cuando se presiona el boton, se pasa al siguiente estado:

- cuando se prende: tira de led apagada
- 1 click: ajuste de color: con el potenciometro varía el color de la tira
- otro click: ajuste de brillo: con el potenciometro varía el brillo o intensidad de la tira
- otro click: ajustes establecidos, color e intensidad definidos
- otro click: se apaga

- doble click: ajuste de intensidad de blanco
- mantener click apretado: cambia color de la primera mitad de la tira
- otro click: cambiar intensidad de la primera mitad
- otro click: cambiar color de la segunda mitad
- otro click: cambiar intensidad de la segunda mitado
- otro click: se apaga

# código:
1. descargar los archivos lampara_led_rgb.ino y events.ino y guardarlos en la misma carpeta.
2. abrir lampara_led_rgb.ino en Arduino IDE (instrucciones para instalar arduino: https://gitlab.com/nicolassaganias/arduino)

# errores frecuentes
- si les aparece un error parecido a este: 
https://gitlab.com/nicolassaganias/lampara-led-rgb/uploads/c17f1a9df6a4687c19222441edd8d4b7/error_events.jpeg
+ info: https://eldesvandejose.com/2016/04/23/incluir-un-sketch-en-otro/

hay que asegurarse de haber descargado el archivo events.ino y que cuando se abra el sketch lampara_led_rgb también se abra al lado, una pestaña "events". si eso no sucede pueden agregarlo manualmente: 
1. dentro del arduino ide, en el sketch lampara_led_rgb.ino hacer click en la flecha que apunta hacia abajo que está justo debajo de la lupa (que abre el monitor del puerto serie) y seleccionar New Tab o Nueva Pestaña o simplemente presionar shift + cmd + N en mac.
2. minimizar arduino y hacer doble click en el sketch events.ino. cuando se abra, en arduino, copiamos todo ese contenido y lo pegamos en la nueva pestaña que hemos creado dentro del sketch lampara_led_rgb y lo salvamos con el nombre events.

# conexión en protoboard
![fritzing](https://gitlab.com/nicolassaganias/lampara-led-rgb/uploads/d4e4fcb7a799124e579e8002fae6dfc5/lampara_led_fritzing_power_tira.png)

# esquemático
![esquemático](https://user-images.githubusercontent.com/74024248/111005625-73b70880-838b-11eb-9542-4cbcb20eab4e.png)

# montaje:
yo monté casi 2 mts de tira de led en un listón de madera y eso lo atornillé a la pared de detrás de mi cama. da muy buena luz para leer.
![fucsia](https://gitlab.com/nicolassaganias/lampara-led-rgb/uploads/c253744989046de60ab19be4fc35a0d3/fucsia.jpeg)
![ambar](https://gitlab.com/nicolassaganias/lampara-led-rgb/uploads/ca2011f72dfd424043e5be602742c7c9/ambar.jpeg)
![verde](https://gitlab.com/nicolassaganias/lampara-led-rgb/uploads/172277d7196e3009eef35cf057e3d27d/verde.jpeg)
![turquesa](https://gitlab.com/nicolassaganias/lampara-led-rgb/uploads/8a4262d0eab2916413d63aade4c39a9e/turquesa.jpeg)
![lampara-led-rgb-1](https://user-images.githubusercontent.com/74024248/111000442-5b8ebb80-8382-11eb-9821-faa2197d5fc5.png)
![lampara-led-rgb-2](https://user-images.githubusercontent.com/74024248/111000431-57629e00-8382-11eb-9071-0bcce20c2007.png)
